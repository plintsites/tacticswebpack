import React from 'react';
import PureRenderMixin from 'react-addons-pure-render-mixin';

/*
    PlayerTurner component: component to update turns
    <PlayerTurner />
 */

var pTImage = require('../images/icon-24-one-finger-tap.png');

const PlayerTurner = React.createClass({
    mixins: [PureRenderMixin],

    propTypes: {
        changeTurn: React.PropTypes.func.isRequired
    },

    render: function() {
        return (
            <div className="turner-container" onClick={this.props.changeTurn}>
                <div className="turner">
                    <img src={pTImage}/>
                </div>
            </div>
        )
    }
});

export default PlayerTurner;

