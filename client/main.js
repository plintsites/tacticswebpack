import React from 'react';
import ReactDOM from 'react-dom';
import Perf from 'react-addons-perf';
import Tactics from './components/Tactics';

// Import css
// import css from './styles/app.less'

window.Perf = Perf;

import 'bootstrap/dist/css/bootstrap.css';
require('./styles/app.less');

ReactDOM.render(<Tactics/>, document.querySelector('#root'));
